/*
 * Product Model Data manager
 */
 import redis from 'redis';
 import request from 'request';
 import bluebird from  'bluebird';
 import "core-js/stable";
 import "regenerator-runtime/runtime";
/*
 * Define const
 */
 const key = process.env.DARKSKY_KEY;
 const redisUrl = process.env.REDIS_URL
 const urlExternalApi = `https://api.darksky.net/forecast`;
 const client = redis.createClient(redisUrl);
 const REDIS_EXPIRATION = 60

 //Insert on Redis
 export const insertRedis = (key, value) => {
     return client.set(`${key}`, value , 'EX', REDIS_EXPIRATION);
 }

 //Get from redis
 export const getFromRedis = (lat,lon) => {
   bluebird.promisifyAll(redis);
   //Return promise
   return new Promise((resolve, reject) => {
     client.getAsync(`${lat}_${lon}`).then((response) => {
       if(response){
         return resolve(response);
       } else {
         return resolve(false);
       }
     });
   });
 }//End function

//Get from external Api
 export const getFromeExternalApi = (lat,lon) => {
   //Return Promise
   return new Promise((resolve, reject) => {
     let request_url = `${urlExternalApi}/${key}/${lat},${lon}`;
     request(request_url, (error, response, body) => {
       insertRedis(`${lat}_${lon}`, body);
       return resolve(body);
     });
  });
} //End function
