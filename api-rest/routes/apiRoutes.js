import { Router } from 'express';
import { weatherTimeController } from '../controllers/apiController';
var apiRoutes = Router();
/*
 * Set Api routes Here
 */
apiRoutes.get('/api/:lat,:lon', weatherTimeController );

export default apiRoutes;
