/*
 *  Api controllers
 */
 import "core-js/stable";
 import "regenerator-runtime/runtime";
 import { getFromRedis, getFromeExternalApi } from '../models/DarkskyModel';
/*
 * Controller Wheather Time DARKSKY
 */
export const weatherTimeController = async (req, res) => {

  if(!req.params.lat || !req.params.lon){
    res.send({data: { msg: "params required"}, error: true});
    return; //exit
  }

  //Get from redis
  let weather = await getFromRedis(req.params.lat, req.params.lon);
  if(!weather){ //Get from external api
    weather = await getFromeExternalApi(req.params.lat, req.params.lon);
    res.send({data: JSON.parse(weather), error: false });
  } else {
    res.send({data: JSON.parse(weather), error: false });
  }

} //End function controller
